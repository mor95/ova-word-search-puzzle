const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected.')
    }

    if(!_.isObject(args.instance)){
        throw new Error('A puzzle instance is expected.')
    }

    _.forEach(args.instance.$highlightedSquares, function(v){
        if(!v.data('complete')){
            v
            .data('selecting', false)
            .removeClass('selecting')
        }
    })
    args.instance.$board.data('selecting', false)
}