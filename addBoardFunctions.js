const _ = require('lodash')
const getDirection = require('./getDirection.js')
const getDirectionBetweenPoints = require('./getDirectionBetweenPoints.js')
const getConnectingPoints = require('./getConnectingPoints.js')
const setStartingPoint = require('./setStartingPoint.js')
const joinPoints = require('./joinPoints.js')
const unhighlightSquares = require('./unhighlightSquares.js')
const completeWords = require('./completeWords.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected')
    }

    if(!_.isObject(args.instance)){
        throw new Error('An instance is expected.');
    }

    if(!(args.instance.$board instanceof jQuery) || !args.instance.$board.length){
        throw new Error('Invalid $board')
    }

    if(!_.isArray(args.instance.elementsGrid)){
        throw new Error('Invalid elementsGrid. An array is expected.')
    }

    args.instance.endPoints = [];
    args.instance.$highlightedSquares = [];
    args.instance.lastSelectedCharacters = [];

    args.instance.$board.find('td').on('mousedown', function(){
        if($(this).data('complete') || $(this).data('selecting')){
            return true;
        }

        setStartingPoint({
            instance: args.instance,
            $element: $(this)
        })
    })
    .on('mousemove', function(){
        if(args.instance.$board.data('selecting')){
            args.instance.endPoints[1] = $(this).data('coords')
            joinPoints({
                instance: args.instance,
                points: args.instance.endPoints,
            })
        }
        else{
            _.forEach(args.instance.$highlightedSquares, function(v){
                v.removeClass('selecting');
            })

            args.instance.$board.find('td').removeClass('selecting')
            $(this).addClass('selecting')
        }
    })

    $('body').on('mouseup', function(){
        if(args.instance.$board.data('selecting')){
            unhighlightSquares({
                instance: args.instance
            })
            
            completeWords({
                instance: args.instance
            })
        }
    })
}