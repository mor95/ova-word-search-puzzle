module.exports = function(c1, c2){
    return {
        verticalUp: c1.x === c2.x && c1.y > c2.y,
        verticalDown: c1.x === c2.x && c1.y < c2.y,
        horizontalRight: c1.x < c2.x && c1.y === c2.y,
        horizontalLeft: c1.x > c2.x && c1.y === c2.y,
        diagonalRightUp: c1.x < c2.x && c1.y > c2.y && Math.abs(c2.x - c1.x) === Math.abs(c2.y - c1.y),
        diagonalRightDown: c1.x < c2.x && c1.y < c2.y && Math.abs(c2.x - c1.x) === Math.abs(c2.y - c1.y),
        diagonalLeftUp: c1.x > c2.x && c1.y > c2.y && Math.abs(c1.x - c2.x) === Math.abs(c1.y - c2.y),
        diagonalLeftDown: c1.x > c2.x && c1.y < c2.y && Math.abs(c1.x - c2.x) === Math.abs(c1.y - c2.y)
    }
}