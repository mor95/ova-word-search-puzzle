const _ = require('lodash')
const appendWords = require('./appendWords.js')
const makePuzzle = require('./makePuzzle.js')
const renderBoard = require('./renderBoard.js')
const addBoardFunctions = require('./addBoardFunctions.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected.')
    }

    if(!_.isArray(args.words)){
        throw new Error('Some words are expected.')
    }

    var self = this;
    self.appendWords = appendWords;
    self.renderBoard = renderBoard;
    self.makePuzzle = makePuzzle;
    self.words = args.words;
    self.appendWords(args);

    self.grid = self.makePuzzle(args);

    var render = self.renderBoard({
        grid: self.grid.raw,
        $boardTarget: args.$boardTarget,
        boardClasses: args.boardClasses
    });

    self.$board = render.$board;
    self.elementsGrid = render.elementsGrid;

    _.forEach(['onWordCompletion', 'onWordMatch', 'onFailedWordMatch', 'onFinish'], function(v){
        if(_.isFunction(args[v])){
            self[v] = args[v];
        }
    })

    addBoardFunctions(_.merge({
        instance: self
    }, args))
}